from __future__ import print_function
import numpy as np
import os, cv2, sys
from glob import glob 
from natsort import natsorted
""" Data load functions. 

Code reference: Most of this code is from Jocic Marko Keras U-Net implementation: https://github.com/jocicmarko/ultrasound-nerve-segmentation. 

"""
# Cache the original image dimensions:
img_rows, img_cols = 420, 580

# Define the directory containing the train and test sets:
data_dir = '/insert data directory here/'

def create_train_file():
	""" Loads all the .tif training images and training masks and saves it in a .npy file for more efficient data loading. 
	"""
	# Create a list of all the training image file paths and the training mask file paths. We use natsorted which does a "human" sorting. 
	train_file_list = natsorted(glob(os.path.join(data_dir, 'train/*.tif')))
	train_img_file_list = [file_path for file_path in train_file_list if 'mask' not in file_path]
	train_mask_file_list = [file_path for file_path in train_file_list if 'mask' in file_path]
	num_train = len(train_img_file_list)

	assert num_train == len(train_mask_file_list), 'Number of training images does not match the number of training masks. Please check your training dataset.'

	print("*"*50)
	print("Loading the training data...")
	print("*"*50)

	train_imgs = np.zeros((num_train, 1, img_rows, img_cols), dtype='uint8')
	train_masks = np.zeros((num_train, 1, img_rows, img_cols), dtype='uint8')
	point = num_train/100
	increment = num_train/50
	for i in xrange(num_train):
		train_imgs[i] = cv2.imread(train_img_file_list[i], cv2.IMREAD_GRAYSCALE)
		train_masks[i] = cv2.imread(train_mask_file_list[i], cv2.IMREAD_GRAYSCALE)

		if(i % (2*point) == 0):
			sys.stdout.write("\r[" + "=" * (i / increment) +  " " * ((num_train - i)/ increment) + "] " +  str(i / point) + "%  ")
			sys.stdout.flush()

	print("\nTraining set load completed.")

	# Save as .npy files:
	np.save(os.path.join(data_dir, 'train_imgs.npy'), train_imgs)
	np.save(os.path.join(data_dir, 'train_masks.npy'), train_masks)
	print("Saving to .npy files completed.")

def create_test_file():
	""" Loads all the .tif test images and saves it in a .npy file for more efficient data loading. 
	"""
	# Create a list of all the test image file paths. We use natsorted which does a "human" sorting. 
	test_file_list = natsorted(glob(os.path.join(data_dir, 'test/*.tif')))
	num_test = len(test_file_list)

	print("*"*50)
	print("Loading the test data...")
	print("*"*50)

	test_imgs = np.zeros((num_test, 1, img_rows, img_cols), dtype='uint8')
	point = num_test/100
	increment = num_test/50
	for i in xrange(num_test):
		test_imgs[i] = cv2.imread(test_file_list[i], cv2.IMREAD_GRAYSCALE)

		if(i % (2*point) == 0):
			sys.stdout.write("\r[" + "=" * (i / increment) +  " " * ((num_test - i)/ increment) + "] " +  str(i / point) + "%  ")
			sys.stdout.flush()

	print("\nTest set load completed.")

	# Save as .npy files:
	np.save(os.path.join(data_dir, 'test_imgs.npy'), test_imgs)
	print("Saving to .npy files completed.")

def load_train():
	""" A shortcut function which will load the training set.
	"""
	X_train = np.load(os.path.join(data_dir, 'train_imgs.npy'))
	Y_train = np.load(os.path.join(data_dir, 'train_masks.npy'))

	return X_train, Y_train

def load_test():
	""" A shortcut function which will load the test set.
	"""
	X_test = np.load(os.path.join(data_dir, 'test_imgs.npy'))

	return X_test

if __name__ == '__main__':
	create_train_file()
	create_test_file()