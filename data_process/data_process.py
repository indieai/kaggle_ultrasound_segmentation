import cv2, sys, theano
import numpy as np

def resize_and_binarize(X, new_shape, mask):
	""" A function that takes as input a 4D tensor and resizes the 2D spatial dimensions. Will rescale data so that its values lie in the unit interval [0,1]. For image data the scaled values will be dense while for mask data, the scaled values will be binarized {0,1}.

	Parameters
	----------

	* X : numpy array type
		A 4D array with shape (N, C, H, W) where:
		Input dataset of shape (N, C, H, W) where:
			* N : number of data points
			* C : number of image channels 
			* H : spatial height of the image 
			* W : spatial width of the image 
		We assume C = 1 and that our training masks are binary valued. 

	* new_shape : tuple type
		A 2-tuple of integers (h,w) indicating the new spatial height and width dimensions of the image. 

	* mask : boolean type
		A boolean True/False indicating whether or not the input represents a set of masks or images.
	"""
	assert len(X.shape)==4, "Input must be a 4D tensor."
	N, C, H, W = X.shape

	h, w = new_shape

	X_resized = np.zeros((N, C, h, w))

	# Resize all the images:
	point = N/100
	increment = N/50
	for i in xrange(N):
		X_resized[i,0] = cv2.resize(X[i, 0], (w, h), interpolation=cv2.INTER_CUBIC)

	if mask:
		X_resized /= 255 
		X_resized = X_resized.astype('int8')
	else:
		X_resized /= 255.0
		X_resized = X_resized.astype(theano.config.floatX)

	return X_resized

def flips(X, mode):
	""" A function that takes as input a 4D tensor and returns a flipped version of the input dataset: either all horiztonal or vertical flips are performed. 

	Parameters
	----------
	* X : numpy array type 
		Input dataset of shape (N, C, H, W) where:
			* N : number of data points
			* C : number of image channels 
			* H : spatial height of the image 
			* W : spatial width of the image 

	* mode : string type 
		String for which types of flips to perform, either "horizontal" or "vertical" 
	"""
	assert(mode == "horizontal" or mode == "vertical"), "Invalid mode selected, options are: 'horizontal' or 'vertical'."

	if mode == "vertical":
		return X[:,:, ::-1, :]
	elif mode == "horizontal":
		return X[:, :, :, ::-1] 

def random_crop(X, Y, crop_size, pad=(0,0)):
	""" A function that takes as input 4D tensors X and Y representing  training images and training masks, respectively, and returns a random crops of each. 

	Parameters
	----------

	* X, Y : numpy array type 
		Input dataset of shape (N, C, H, W) where:
			* N : number of data points
			* C : number of image channels 
			* H : spatial height of the image 
			* W : spatial width of the image 

	* crop_size : tuple type 
		Crop size. 

	* pad: tuple type
		Spatial padding. Default is set to (0,0), i.e. no padding.
	"""
	# Pad the data:
	if pad != (0,0):
		X = np.lib.pad(X, ((0,0),(0,0),(pad[0],pad[0]), (pad[1],pad[1])), mode='constant')
		Y = np.lib.pad(Y, ((0,0),(0,0),(pad[0],pad[0]), (pad[1],pad[1])), mode='constant')

	N, C, H, W = X.shape 
	h, w = crop_size

	assert (h < H and w < W), "Crop dimensions cannot exceed original image dimensions." 

	X_random_crops = np.zeros((N,C,h,w), dtype=theano.config.floatX)
	Y_random_crops = np.zeros((N,C,h,w), dtype='int8')

	# Choose a random corner point in a vectorized fashion:
	corners = np.random.random_integers(low=0, high=H-h, size=(N,2))

	for i in xrange(N):
		X_random_crops[i] = X[i, :, corners[i,0]:corners[i,0]+h, corners[i,1]:corners[i,1]+w]
		Y_random_crops[i] = Y[i, :, corners[i,0]:corners[i,0]+h, corners[i,1]:corners[i,1]+w]

	return X_random_crops, Y_random_crops

def full_data_augmentation(X_train, Y_train, X_test, val_ratio, new_shape, aux_shapes):
	""" A helper function which takes our training set consisting of X_train and Y_train and our test set consisting of X_test and performs the following operations:

		* Resizes X_train, Y_train, X_test
		* Creates a validation set X_val, Y_val
		* Performs global contrast normalization
		* Augments X_train with vertical and horizontal flips
		* Augments X_train with random crops of a different resized version of the training set. 

	Parameters
	----------

	* X_train, Y_train, X_test : numpy array types.
		Each of these are 4D tensors of shape (N, C, H, W) where:
			* N : number of data points, respectively
			* C : number of image channels 
			* H : spatial height of the image 
			* W : spatial width of the image 

		X_train represents the training images.
		Y_train represents the training masks.
		X_test represent the test images. 

	* val_ratio : float type 
		A float specifying percentage of the training set to use as a validation set. 

	* new_shape : tuple type
		A 2-tuple of integers (h,w) indicating the new spatial height and width dimensions of the image. 

	* aux_shapes : list of tuple type
		A list of 2-tuples of integers (h,w) indicating the aux resized spatial height and width dimensions of the image from which we will extract new_shape sized random crops.
	"""

	# Resize X_train, Y_train, X_test to new_shape:
	X_train_rsz = resize_and_binarize(X_train, new_shape, False)
	Y_train_rsz = resize_and_binarize(Y_train, new_shape, True)
	X_test_rsz = resize_and_binarize(X_test, new_shape, False)

	# Create a validation set: 
	num_train = np.ceil((1-val_ratio)*X_train.shape[0])
	X_train_rsz, X_val_rsz = X_train_rsz[:num_train], X_train_rsz[num_train:]
	Y_train_rsz, Y_val_rsz = Y_train_rsz[:num_train], Y_train_rsz[num_train:]

	# Global Contrast Normalization: 
	train_mean, train_std = X_train_rsz.mean(axis=0), X_train_rsz.std(axis=0)
	X_train_rsz -= train_mean 
	X_train_rsz /= train_std

	X_val_rsz -= train_mean
	X_val_rsz /= train_std

	X_test_rsz -= train_mean
	X_test_rsz /= train_std

	# Augment with horizontal and vertical flips:
	X_train_rsz = np.concatenate((X_train_rsz, flips(X_train_rsz, 'horizontal') , flips(X_train_rsz, 'vertical') ),axis=0)
	Y_train_rsz = np.concatenate((Y_train_rsz, flips(Y_train_rsz, 'horizontal') , flips(Y_train_rsz, 'vertical') ),axis=0)

	for aux_shape in aux_shapes:
		# Augment with random crops from differently resized images:
		X_train_rsz_aux = resize_and_binarize(X_train[:num_train], aux_shape, False)
		Y_train_rsz_aux = resize_and_binarize(Y_train[:num_train], aux_shape, True)

		# Global Contrast Normalization on the auxiliary resized training set:
		train_mean_aux, train_std_aux = X_train_rsz_aux.mean(axis=0), X_train_rsz_aux.std(axis=0)
		X_train_rsz_aux -= train_mean_aux
		X_train_rsz_aux /= train_std_aux

		# Extract random new_shaped-sized crops:
		X_train_rsz_aux_crops, Y_train_rsz_aux_crops = random_crop(X_train_rsz_aux, Y_train_rsz_aux, new_shape)

		# Augment using random crops and horizontal and vertical flips of those random crops:
		X_train_rsz = np.concatenate((X_train_rsz, X_train_rsz_aux_crops, flips(X_train_rsz_aux_crops, 'horizontal'), flips(X_train_rsz_aux_crops, 'vertical')), axis=0)
		Y_train_rsz = np.concatenate((Y_train_rsz, Y_train_rsz_aux_crops, flips(Y_train_rsz_aux_crops, 'horizontal'), flips(Y_train_rsz_aux_crops, 'vertical')), axis=0)

	return X_train_rsz, Y_train_rsz, X_val_rsz, Y_val_rsz, X_test_rsz

def no_aux_resize_augmentation(X_train, Y_train, X_test, new_shape, augment):
	""" A helper function which takes our training set consisting of X_train and Y_train and our test set consisting of X_test and performs the following operations:

		* Resizes X_train, Y_train, X_test
		* Performs global contrast normalization
		* Data augmentation with horizontal and vertical flips

	Parameters
	----------

	* X_train, Y_train, X_test : numpy array types.
		Each of these are 4D tensors of shape (N, C, H, W) where:
			* N : number of data points, respectively
			* C : number of image channels 
			* H : spatial height of the image 
			* W : spatial width of the image 

		X_train represents the training images.
		Y_train represents the training masks.
		X_test represent the test images. 

	* new_shape : tuple type
		A 2-tuple of integers (h,w) indicating the new spatial height and width dimensions of the image. 

	* augment : Boolean type 
		A boolean True/False indicating whether or not to augment the training data using horizontal and vertical flips.

	"""

	# Resize X_train, Y_train, X_test to new_shape:
	X_train_rsz = resize_and_binarize(X_train, new_shape, False)
	Y_train_rsz = resize_and_binarize(Y_train, new_shape, True)
	X_test_rsz = resize_and_binarize(X_test, new_shape, False)

	# Global Contrast Normalization: 
	train_mean, train_std = X_train_rsz.mean(axis=0), X_train_rsz.std(axis=0)
	X_train_rsz -= train_mean 
	X_train_rsz /= train_std

	X_test_rsz -= train_mean
	X_test_rsz /= train_std

	# Augment with horizontal and vertical flips:
	if augment:
		X_train_rsz = np.concatenate((X_train_rsz, flips(X_train_rsz, 'horizontal') , flips(X_train_rsz, 'vertical') ),axis=0)
		Y_train_rsz = np.concatenate((Y_train_rsz, flips(Y_train_rsz, 'horizontal') , flips(Y_train_rsz, 'vertical') ),axis=0)

	return X_train_rsz, Y_train_rsz, X_test_rsz

if __name__ == '__main__':
	pass 