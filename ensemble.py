""" Ensembles the U-Net-Maxout-BN network and the U-Net-Inception-BN network.''
"""
from data_process.data_load import load_test, resize_and_binarize
from utils.utils import *
from models.networks import build_baseline_unet_inception_bn
from models.networks import build_baseline_unet_maxout_bn
import theano, os, sys, csv, cv2
import theano.tensor as T
from lasagne.layers import get_output, get_all_params, set_all_param_values

# Define the best validation indices for U-Net-Maxout-BN and U-Net-Inception-BN:
maxout_index = 0 # Set this manually 
inception_index = 0 # Set this manually 

# Define some parameters:
h_reshape, w_reshape = (64, 80)
input_shape = (1, h_reshape, w_reshape)
p_drop = 0.5

# Load the test data:
X_test = load_test()
N, C, H, W = X_test.shape

# Resize the test set:
X_test = resize_and_binarize(X=X_test, new_shape=(h_reshape, w_reshape), mask=False)

# Define the weights directory where all the weights are saved:
# weights_dir = '/insert_weights_dir/'
inception_weights_dir = weights_dir+'unet_inception_bn/'
maxout_weights_dir = weights_dir+'unet_maxout_bn/'

# Initialize Theano symbolic variables:
input_var = T.tensor4('inputs')
target_var = T.tensor4('targets', dtype='int8')

# Initialize the U-Net-Inception-BN network:
inception_net = build_baseline_unet_inception_bn(input_var, input_shape, p_drop)

# Load the weights:
with np.load(inception_weights_dir+'%d_epochs_completed_weights.npz' % (inception_index)) as f:
	param_values = [f['arr_%d' % i] for i in range(len(f.files))]
set_all_param_values(inception_net['seg_prob'], param_values)

# Create a U-Net-Inception-BN Theano prediction function:
inception_predictions = get_output(inception_net['seg_prob'], deterministic=True)
get_inception_predictions = theano.function([input_var], inception_predictions)

# Initialize the U-Net-Maxout-BN network:
maxout_net = build_baseline_unet_maxout_bn(input_var, input_shape, p_drop)

# Load the weights:
with np.load(maxout_weights_dir+'%d_epochs_completed_weights.npz' % (maxout_index)) as f:
	param_values = [f['arr_%d' % i] for i in range(len(f.files))]
set_all_param_values(maxout_net['seg_prob'], param_values)

# Compile a U-Net-Maxout-BN Theano prediction function:
maxout_predictions = get_output(maxout_net['seg_prob'], deterministic=True)
get_maxout_predictions = theano.function([input_var], maxout_predictions)

print("*"*50)
print("Predicting and ensembling on the test set...")
print("*"*50)
# Predict on the test set and average the masks together:
predicted_masks = np.zeros((0,C,h_reshape,w_reshape), dtype='uint8')
Y_test_dummy = np.zeros((N,C,h_reshape,w_reshape), dtype='uint8')
for batch in iterate_minibatches(X_test, Y_test_dummy, 36, shuffle=False):
	test_img_batch, test_mask_dummy_batch = batch
	predicted_masks = np.concatenate((predicted_masks, np.mean([get_inception_predictions(test_img_batch), get_maxout_predictions(test_img_batch)], axis=0)),axis=0)

print("*"*50)
print("Preparing a Kaggle submission...")
print("*"*50)
point = N/100
increment = N/50
# Convert the predicted test masks into run length encoding format:
with open(os.path.join(weights_dir, 'maxout_inception_ensemble_submission.csv'), 'wb') as csvfile:
	kaggle_submission = csv.writer(csvfile, delimiter=',')
	kaggle_submission.writerow(['img', 'pixels'])
	for index, img in enumerate(predicted_masks):
		img = img.squeeze()
		img = cv2.threshold(img, 0.5, 1., cv2.THRESH_BINARY)[1].astype('uint8')
		img = cv2.resize(img, (W, H), interpolation=cv2.INTER_CUBIC)
		rle = run_length_enc(img)

		# Write to the submission.csv file:
		kaggle_submission.writerow(['%d' % (index+1), rle])

		if(index % (2*point) == 0):
			sys.stdout.write("  Test image %d of %d \r[" % (index+1, N) + "=" * (index / increment) +  " " * ((N - index)/ increment) + "] " +  str(index / point) + "% ")
			sys.stdout.flush()

	print("Kaggle CSV submission file ready.")