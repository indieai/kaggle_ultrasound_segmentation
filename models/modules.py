import theano
import theano.tensor as T
from collections import OrderedDict 
from lasagne.layers import InputLayer, Conv2DLayer, BatchNormLayer
from lasagne.layers import NonlinearityLayer, ConcatLayer, ExpressionLayer
from lasagne.layers import PadLayer, ElemwiseSumLayer, GlobalPoolLayer
from lasagne.layers import DenseLayer, MaxPool2DLayer, dropout, Layer
from lasagne.nonlinearities import rectify, linear, softmax, sigmoid
from lasagne.init import Normal, HeNormal, GlorotUniform

###############################################################################
########################### Custom Lasagne Layers #############################
###############################################################################

class FeaturePermutationLayer(Layer):
	""" This layer performs a random permutation in the feature dimension of a 4D tensor with shape (N, F, H, W) where:
		* N is the batchsize/number of data points
		* F is the number of filters
		* H, W are the spatial dimensions

	"""
	def __init__(self, incoming, seed=1234, **kwargs):
		super(FeaturePermutationLayer, self).__init__(incoming, **kwargs)
		
		# The number of filters in the input:
		self.num_filters = list(incoming.output_shape)[1]

		# Generate a random permutation and cache it. The only randomness is the randomly initialized permutation, and it never changes afterwards:
		self.seed = seed
		self.srng = RandomStreams(seed=self.seed)
		self.permutation = self.srng.permutation(n=self.num_filters, size=(1,))

	def get_output_for(self, input, **kwargs):
		# Return the filter-permuted input:
		return input[:, self.permutation, :, :].squeeze()

		#######################
		##### ! Warning ! #####
		#######################

		# Need to raise an error if any of the input shapes are 1, otherwise this will really screw up my code. 

	def get_output_shape_for(self, input_shape):
		return input_shape

###############################################################################
################################## Modules ####################################
###############################################################################

def build_conv_bn_layer(incoming, name, num_filters, filter_size, stride, pad, W_init, nonlinearity=rectify):
	""" Constructs a Conv2D layer with batch normalization applied before the nonlinearity. 

	Parameters
	----------

	* incoming : a lasagne.layers.Layer class instance.
		The layer that is being fed into this layer.

	* name : string 
		A string specifying the name of the network layer/module, e.g. 'conv1'.

	* num_filters : int 
		An integer specifying the number of convolutional filters this layer has.

	* filter_size : int
		An integer specifying the receptive field size of each of the filters.

	* stride : int
		An integer specifying the stride of the convolution operation.

	* pad : int, or 'full' or 'same' or 'valid'
		An integer which specifies the amount of padding to use. See Lasagne documentation on Conv2DLayers for further reference. 

	* W_init : Theano shared variable, expression, numpy array or callable 
		Initial value for the weights of the convolutional filters. 

	* nonlinearity : callable or None 
		Specifies the nonlinearity applied to the output of the convolution operation. Default is set to 'rectify'.

	Example
	-------

	Example output of this function. let name = 'conv1'. This function returns an OrderedDict with the following keys:

		(1) 'conv1.conv' : the raw output of the convolutional layer.
		(2) 'conv1.bn' : the output of the batch normalization layer. 
		(3) 'conv1.nonlinearity' : the output after we apply the nonlinearity 					 	 to the batch normalization output.
	"""

	out = OrderedDict()

	# List of all the keys in 'name.layer' format, we do this for convenience and readability of code. 
	keys = ['{}.{}'.format(name,'conv'),
			'{}.{}'.format(name,'bn'), 
			'{}.{}'.format(name,'nonlinearity')] 

	out[keys[0]] = Conv2DLayer(	incoming=incoming, 
								num_filters=num_filters,
								filter_size=filter_size,
								stride=stride,
								pad=pad,
								W=W_init,
								b=None,
								nonlinearity=linear)

	out[keys[1]] = BatchNormLayer(out[keys[0]])

	out[keys[2]] = NonlinearityLayer(incoming=out[keys[1]], 
									 nonlinearity=nonlinearity)

	return out

def build_multi_scale_inception_bn_layer(incoming, name, num_filters, filter_sizes, strides, pads, W_init, nonlinearity=rectify):
	""" Implements a version of the Google Research Inception architecture: constructs a layer which is a concatenation of Conv-BN modules of differing receptive field sizes. We do not include max-pooling layers in our Inception modules. 

	Parameters
	----------

	* incoming : a lasagne.layers.Layer class instance.
		The layer that is being fed into this layer.

	* name : string 
		A string specifying the name of the network layer/module, e.g. 'conv1'.

	* num_filters : list of ints
		A list of integers specifying the number of convolutional filters for each scale.

	* filter_sizes : list of ints
		A list of integers specifying the receptive field sizes of each convolutional layer, e.g. the different spatial scales. 

		Example: [3,5] would correspond to an Inception layer which is the concatenation of two different convolutional layers, one with receptive field size (3,3) and the other with receptive field size (5,5).

	* strides : list of ints, or list of iterable of int
		A list of ints or a list of 2-tuples of ints specifying the stride of each of the convolution operations.

	* pads : list of ints, or iterables of int, 'full', 'same' or 'valid'
		A list of integers specifying the padding for each of the convolutional layers.

	* W_init : Theano shared variable, expression, numpy array or callable 
		Initial value for the weights of the convolutional filters. 

	* nonlinearity : callable or None 
		Specifies the nonlinearity applied to the output of the convolution operation. Default is set to 'rectify'.

	Example
	-------

	Example output of this function: let name = 'Inception1' and filter_sizes=[3,5], so this is an Inception layer which will be the concatenation of a 3x3 conv layer and a 5x5 conv layer. This function will return an OrderedDict with the following keys:

		(1) 'Inception1.conv3.conv' : the raw output of the 3x3 convolutional 							layer.

		(2) 'Inception1.conv3.bn'	: the output of the batch normalization 						  layer of the conv-3x3 output.

		(3) 'Inception1.conv3.nonlinearity' : the output after we apply the 								  nonlinearity to the batch 									  normalized conv-3x3 output.

		(4) 'Inception1.conv5.conv' : the raw output of the 5x5 convolutional 							layer.

		(5) 'Inception1.conv5.bn'	: the output of the batch normalization 						  layer of the conv-5x5 output .

		(6) 'Inception1.conv5.nonlinearity' : the output after we apply the 								  nonlinearity to the batch 									  normalized conv-5x5 output

		(7)	'Inception1.concat' : the concatenation of 	the two layers							  'Inception1.conv3.nonlinearity' and 							  'Inception1.conv3.nonlinearity'


	Reference
	---------
	Title: 		'Going Deeper with Convolutions'

	Authors: 	Christian Szegedy, Wei Liu, Yangqing Jia, Pierre Sermanet, 
				Scott Reed, Dragomir Anguelov, Dumitru Erhan, 
				Vincent Vanhoucke, Andrew Rabinovich

	arXiv: 		http://arxiv.org/abs/1409.4842

	"""
	# Ensure that the size of the parameter lists all match:
	assert (len(num_filters)==len(filter_sizes)==len(strides)==len(pads)), "Number of Inception Layers do not match. Check to make sure the length of the num_filters, filter_size, stride, pad lists all agree."

	out = OrderedDict()

	num_layers = len(num_filters)

	# Create each of the convolutional layers:
	for i in xrange(num_layers):
		out.update(
			build_conv_bn_layer(
				incoming=incoming, 
				name='{}.{}'.format(name, 'conv'+str(filter_sizes[i])), 
				num_filters=num_filters[i], 
				filter_size=filter_sizes[i], 
				stride=strides[i], 
				pad=pads[i], 
				W_init=W_init, 
				nonlinearity=rectify))

	# Concatenate all the outputs
	out['{}.{}'.format(name, 'concat')] = ConcatLayer([out['{}.{}.{}'.format(name, 'conv'+str(filter_sizes[i]), 'nonlinearity')] for i in xrange(num_layers)])

	return out 

if __name__ == '__main__':
	pass