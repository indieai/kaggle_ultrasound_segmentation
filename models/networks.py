from modules import *
from lasagne.layers import MaxPool2DLayer, Upscale2DLayer, FeaturePoolLayer
from lasagne.layers import ElemwiseSumLayer, BatchNormLayer

W_init = GlorotUniform() # Glorot Init
# W_init = HeNormal(gain='relu') # HeNormal Init

###############################################################################
############################# Sigmoidal Networks ##############################
###############################################################################

def build_baseline_unet(input_var, input_shape, p_drop, W_init=W_init):
	""" A so-called all convolutional U-Net which we will use as a baseline as well as to make sure all our code is functional. 

	Parameters
	----------

	* input_var : Theano tensor4 symbolic variable.
		A Theano symbolic variable representing 4D float32 input.

	* input_shape : tuple type
		A 3-tuple of integers (C, H, W) where:
			* C : number of input channels
			* H : spatial height dimension
			* W : spatial width dimension 

	* p_drop : float type 
		A float type representing the amount of dropout to use for the input to the final sigmoidal classification layer.

	* W_init : Theano shared variable, expression, numpy array or callable 
		Initial value for the weights of the convolutional filters. 

	References
	----------

	* U-Net: Convolutional Networks for Biomedical Image Segmentation by Olaf Ronneberger, Philipp Fischer, Thomas Brox. arXiv: https://arxiv.org/abs/1505.04597

	* Jocic Marko Keras U-Net implementation: https://github.com/jocicmarko/ultrasound-nerve-segmentation. 

	"""
	C, H, W = input_shape 

	net = OrderedDict()

	net['input'] = InputLayer(shape=(None,C,H,W), input_var=input_var)
	net['conv1'] = Conv2DLayer(incoming=net['input'], num_filters=32, filter_size=3, stride=1, pad='same', W=W_init, nonlinearity=rectify)
	net['conv2'] = Conv2DLayer(incoming=net['conv1'], num_filters=32, filter_size=3, stride=1, pad='same', W=W_init, nonlinearity=rectify)
	net['pool1'] = MaxPool2DLayer(incoming=net['conv2'], pool_size=(2,2))
	net['conv3'] = Conv2DLayer(incoming=net['pool1'], num_filters=64, filter_size=3, stride=1, pad='same', W=W_init, nonlinearity=rectify)
	net['conv4'] = Conv2DLayer(incoming=net['conv3'], num_filters=64, filter_size=3, stride=1, pad='same', W=W_init, nonlinearity=rectify)
	net['pool2'] = MaxPool2DLayer(incoming=net['conv4'], pool_size=(2,2))	
	net['conv5'] = Conv2DLayer(incoming=net['pool2'], num_filters=128, filter_size=3, stride=1, pad='same', W=W_init, nonlinearity=rectify)
	net['conv6'] = Conv2DLayer(incoming=net['conv5'], num_filters=128, filter_size=3, stride=1, pad='same', W=W_init, nonlinearity=rectify)
	net['pool3'] = MaxPool2DLayer(incoming=net['conv6'], pool_size=(2,2))
	net['conv7'] = Conv2DLayer(incoming=net['pool3'], num_filters=256, filter_size=3, stride=1, pad='same', W=W_init, nonlinearity=rectify)
	net['conv8'] = Conv2DLayer(incoming=net['conv7'], num_filters=256, filter_size=3, stride=1, pad='same', W=W_init, nonlinearity=rectify)
	net['pool4'] = MaxPool2DLayer(incoming=net['conv8'], pool_size=(2,2))
	net['conv9'] = Conv2DLayer(incoming=net['pool4'], num_filters=512, filter_size=3, stride=1, pad='same', W=W_init, nonlinearity=rectify)
	net['conv10'] = Conv2DLayer(incoming=net['conv9'], num_filters=512, filter_size=3, stride=1, pad='same', W=W_init, nonlinearity=rectify)
	net['unpool1'] = Upscale2DLayer(incoming=net['conv10'], scale_factor=2)
	net['concat1'] = ConcatLayer(incomings=[net['unpool1'], net['conv8']])
	net['conv11'] = Conv2DLayer(incoming=net['concat1'], num_filters=256, filter_size=3, stride=1, pad='same', W=W_init, nonlinearity=rectify)
	net['conv12'] = Conv2DLayer(incoming=net['conv11'], num_filters=256, filter_size=3, stride=1, pad='same', W=W_init, nonlinearity=rectify)
	net['unpool2'] = Upscale2DLayer(incoming=net['conv12'], scale_factor=2)
	net['concat2'] = ConcatLayer(incomings=[net['unpool2'], net['conv6']])
	net['conv13'] = Conv2DLayer(incoming=net['concat2'], num_filters=128, filter_size=3, stride=1, pad='same', W=W_init, nonlinearity=rectify)
	net['conv14'] = Conv2DLayer(incoming=net['conv13'], num_filters=128, filter_size=3, stride=1, pad='same', W=W_init, nonlinearity=rectify)
	net['unpool3'] = Upscale2DLayer(incoming=net['conv14'], scale_factor=2)
	net['concat3'] = ConcatLayer(incomings=[net['unpool3'], net['conv4']])
	net['conv15'] = Conv2DLayer(incoming=net['concat3'], num_filters=64, filter_size=3, stride=1, pad='same', W=W_init, nonlinearity=rectify)
	net['conv16'] = Conv2DLayer(incoming=net['conv15'], num_filters=64, filter_size=3, stride=1, pad='same', W=W_init, nonlinearity=rectify)
	net['unpool4'] = Upscale2DLayer(incoming=net['conv16'], scale_factor=2)
	net['concat4'] = ConcatLayer(incomings=[net['unpool4'], net['conv2']])
	net['conv17'] = Conv2DLayer(incoming=net['concat4'], num_filters=32, filter_size=3, stride=1, pad='same', W=W_init, nonlinearity=rectify)
	net['seg_prob'] = Conv2DLayer(incoming=dropout(net['conv17'], p=p_drop), num_filters=1, filter_size=1, stride=1, pad='same', W=W_init, nonlinearity=sigmoid)

	return net

def build_baseline_unet_bn(input_var, input_shape, p_drop, W_init=W_init):
	""" Builds the baseline U-Net model with Batch Normalization. 

	* Note: We do not perform batch normalization for the sigmoidal layer. As the segmentation can occur in different spatial locations for different images. Thus averaging over the elements of the each (which is a component of batch normalization) may degrade the segmentation signal. 

	Parameters
	----------

	* input_var : Theano tensor4 symbolic variable.
		A Theano symbolic variable representing 4D float32 input.

	* input_shape : tuple type
		A 3-tuple of integers (C, H, W) where:
			* C : number of input channels
			* H : spatial height dimension
			* W : spatial width dimension 

	* p_drop : float type 
		A float type representing the amount of dropout to use for the input to the final sigmoidal classification layer.

	* W_init : Theano shared variable, expression, numpy array or callable 
		Initial value for the weights of the convolutional filters. 

	"""
	C, H, W = input_shape 

	net = OrderedDict()

	net['input'] = InputLayer(shape=(None,C,H,W), input_var=input_var)
	net.update(build_conv_bn_layer(incoming=net['input'], name='conv1', num_filters=32, filter_size=3, stride=1, pad='same', W_init=W_init))
	net.update(build_conv_bn_layer(incoming=net['conv1.nonlinearity'], name='conv2', num_filters=32, filter_size=3, stride=1, pad='same', W_init=W_init))
	net['pool1'] = MaxPool2DLayer(incoming=net['conv2.nonlinearity'], pool_size=(2,2))
	net.update(build_conv_bn_layer(incoming=net['pool1'], name='conv3', num_filters=64, filter_size=3, stride=1, pad='same', W_init=W_init))
	net.update(build_conv_bn_layer(incoming=net['conv3.nonlinearity'], name='conv4', num_filters=64, filter_size=3, stride=1, pad='same', W_init=W_init))
	net['pool2'] = MaxPool2DLayer(incoming=net['conv4.nonlinearity'], pool_size=(2,2))	
	net.update(build_conv_bn_layer(incoming=net['pool2'], name='conv5', num_filters=128, filter_size=3, stride=1, pad='same', W_init=W_init))
	net.update(build_conv_bn_layer(incoming=net['conv5.nonlinearity'], name='conv6', num_filters=128, filter_size=3, stride=1, pad='same', W_init=W_init))
	net['pool3'] = MaxPool2DLayer(incoming=net['conv6.nonlinearity'], pool_size=(2,2))
	net.update(build_conv_bn_layer(incoming=net['pool3'], name='conv7', num_filters=256, filter_size=3, stride=1, pad='same', W_init=W_init))
	net.update(build_conv_bn_layer(incoming=net['conv7.nonlinearity'], name='conv8', num_filters=256, filter_size=3, stride=1, pad='same', W_init=W_init))
	net['pool4'] = MaxPool2DLayer(incoming=net['conv8.nonlinearity'], pool_size=(2,2))
	net.update(build_conv_bn_layer(incoming=net['pool4'], name='conv9', num_filters=512, filter_size=3, stride=1, pad='same', W_init=W_init))
	net.update(build_conv_bn_layer(incoming=net['conv9.nonlinearity'], name='conv10', num_filters=512, filter_size=3, stride=1, pad='same', W_init=W_init))
	net['unpool1'] = Upscale2DLayer(incoming=net['conv10.nonlinearity'], scale_factor=2)
	net['concat1'] = ConcatLayer(incomings=[net['unpool1'], net['conv8.nonlinearity']])
	net.update(build_conv_bn_layer(incoming=net['concat1'], name='conv11', num_filters=256, filter_size=3, stride=1, pad='same', W_init=W_init))
	net.update(build_conv_bn_layer(incoming=net['conv11.nonlinearity'], name='conv12', num_filters=256, filter_size=3, stride=1, pad='same', W_init=W_init))
	net['unpool2'] = Upscale2DLayer(incoming=net['conv12.nonlinearity'], scale_factor=2)
	net['concat2'] = ConcatLayer(incomings=[net['unpool2'], net['conv6.nonlinearity']])
	net.update(build_conv_bn_layer(incoming=net['concat2'], name='conv13', num_filters=128, filter_size=3, stride=1, pad='same', W_init=W_init))
	net.update(build_conv_bn_layer(incoming=net['conv13.nonlinearity'], name='conv14', num_filters=128, filter_size=3, stride=1, pad='same', W_init=W_init))
	net['unpool3'] = Upscale2DLayer(incoming=net['conv14.nonlinearity'], scale_factor=2)
	net['concat3'] = ConcatLayer(incomings=[net['unpool3'], net['conv4.nonlinearity']])
	net.update(build_conv_bn_layer(incoming=net['concat3'], name='conv15', num_filters=64, filter_size=3, stride=1, pad='same', W_init=W_init))
	net.update(build_conv_bn_layer(incoming=net['conv15.nonlinearity'], name='conv16', num_filters=64, filter_size=3, stride=1, pad='same', W_init=W_init))
	net['unpool4'] = Upscale2DLayer(incoming=net['conv16.nonlinearity'], scale_factor=2)
	net['concat4'] = ConcatLayer(incomings=[net['unpool4'], net['conv2.nonlinearity']])
	net.update(build_conv_bn_layer(incoming=net['concat4'], name='conv17', num_filters=32, filter_size=3, stride=1, pad='same', W_init=W_init))
	net['seg_prob'] = Conv2DLayer(incoming=dropout(net['conv17.nonlinearity'], p=p_drop), num_filters=1, filter_size=1, stride=1, pad='same', W=W_init, nonlinearity=sigmoid)

	return net

def build_baseline_unet_bn_sigmoidal_pooling(input_var, input_shape, p_drop, W_init=W_init, pool_type='max'):
	""" Builds the baseline U-Net model with batch normalization and we pool together 5 different sigmoidal pixel classifiers (either max or avg).

	* Note: after some exploratory benchmarking, the sigmoidal pooling seems to lead to overfitting and does not appear to be beneficial. We include this network for completeness sake.

	Parameters
	----------

	* input_var : Theano tensor4 symbolic variable.
		A Theano symbolic variable representing 4D float32 input.

	* input_shape : tuple type
		A 3-tuple of integers (C, H, W) where:
			* C : number of input channels
			* H : spatial height dimension
			* W : spatial width dimension 

	* p_drop : float type 
		A float type representing the amount of dropout to use for the input to the final sigmoidal classification layer.

	* W_init : Theano shared variable, expression, numpy array or callable 
		Initial value for the weights of the convolutional filters.

	* pool_type : string type
		A string type indicating whether to use 'max' or 'avg' sigmoidal pooling. Only valid options are 'max' or 'avg'.

	"""
	C, H, W = input_shape 

	net = OrderedDict()

	net['input'] = InputLayer(shape=(None,C,H,W), input_var=input_var)
	net.update(build_conv_bn_layer(incoming=net['input'], name='conv1', num_filters=32, filter_size=3, stride=1, pad='same', W_init=W_init))
	net.update(build_conv_bn_layer(incoming=net['conv1.nonlinearity'], name='conv2', num_filters=32, filter_size=3, stride=1, pad='same', W_init=W_init))
	net['pool1'] = MaxPool2DLayer(incoming=net['conv2.nonlinearity'], pool_size=(2,2))
	net.update(build_conv_bn_layer(incoming=net['pool1'], name='conv3', num_filters=64, filter_size=3, stride=1, pad='same', W_init=W_init))
	net.update(build_conv_bn_layer(incoming=net['conv3.nonlinearity'], name='conv4', num_filters=64, filter_size=3, stride=1, pad='same', W_init=W_init))
	net['pool2'] = MaxPool2DLayer(incoming=net['conv4.nonlinearity'], pool_size=(2,2))	
	net.update(build_conv_bn_layer(incoming=net['pool2'], name='conv5', num_filters=128, filter_size=3, stride=1, pad='same', W_init=W_init))
	net.update(build_conv_bn_layer(incoming=net['conv5.nonlinearity'], name='conv6', num_filters=128, filter_size=3, stride=1, pad='same', W_init=W_init))
	net['pool3'] = MaxPool2DLayer(incoming=net['conv6.nonlinearity'], pool_size=(2,2))
	net.update(build_conv_bn_layer(incoming=net['pool3'], name='conv7', num_filters=256, filter_size=3, stride=1, pad='same', W_init=W_init))
	net.update(build_conv_bn_layer(incoming=net['conv7.nonlinearity'], name='conv8', num_filters=256, filter_size=3, stride=1, pad='same', W_init=W_init))
	net['pool4'] = MaxPool2DLayer(incoming=net['conv8.nonlinearity'], pool_size=(2,2))
	net.update(build_conv_bn_layer(incoming=net['pool4'], name='conv9', num_filters=512, filter_size=3, stride=1, pad='same', W_init=W_init))
	net.update(build_conv_bn_layer(incoming=net['conv9.nonlinearity'], name='conv10', num_filters=512, filter_size=3, stride=1, pad='same', W_init=W_init))
	net['unpool1'] = Upscale2DLayer(incoming=net['conv10.nonlinearity'], scale_factor=2)
	net['concat1'] = ConcatLayer(incomings=[net['unpool1'], net['conv8.nonlinearity']])
	net.update(build_conv_bn_layer(incoming=net['concat1'], name='conv11', num_filters=256, filter_size=3, stride=1, pad='same', W_init=W_init))
	net.update(build_conv_bn_layer(incoming=net['conv11.nonlinearity'], name='conv12', num_filters=256, filter_size=3, stride=1, pad='same', W_init=W_init))
	net['unpool2'] = Upscale2DLayer(incoming=net['conv12.nonlinearity'], scale_factor=2)
	net['concat2'] = ConcatLayer(incomings=[net['unpool2'], net['conv6.nonlinearity']])
	net.update(build_conv_bn_layer(incoming=net['concat2'], name='conv13', num_filters=128, filter_size=3, stride=1, pad='same', W_init=W_init))
	net.update(build_conv_bn_layer(incoming=net['conv13.nonlinearity'], name='conv14', num_filters=128, filter_size=3, stride=1, pad='same', W_init=W_init))
	net['unpool3'] = Upscale2DLayer(incoming=net['conv14.nonlinearity'], scale_factor=2)
	net['concat3'] = ConcatLayer(incomings=[net['unpool3'], net['conv4.nonlinearity']])
	net.update(build_conv_bn_layer(incoming=net['concat3'], name='conv15', num_filters=64, filter_size=3, stride=1, pad='same', W_init=W_init))
	net.update(build_conv_bn_layer(incoming=net['conv15.nonlinearity'], name='conv16', num_filters=64, filter_size=3, stride=1, pad='same', W_init=W_init))
	net['unpool4'] = Upscale2DLayer(incoming=net['conv16.nonlinearity'], scale_factor=2)
	net['concat4'] = ConcatLayer(incomings=[net['unpool4'], net['conv2.nonlinearity']])
	net.update(build_conv_bn_layer(incoming=net['concat4'], name='conv17', num_filters=32, filter_size=3, stride=1, pad='same', W_init=W_init))
	net['conv18'] = Conv2DLayer(incoming=dropout(net['conv17.nonlinearity'], p=p_drop), num_filters=5, filter_size=1, stride=1, pad='same', W=W_init, nonlinearity=sigmoid)
	if pool_type == 'max':
		net['seg_prob'] = FeaturePoolLayer(incoming=net['conv18'], pool_size=5, pool_function=theano.tensor.max) # Max pooling
	elif pool_type == 'avg':
		net['seg_prob'] = FeaturePoolLayer(incoming=net['conv18'], pool_size=5, pool_function=theano.tensor.mean) # Avg pooling
	
	return net

def build_baseline_unet_maxout_bn(input_var, input_shape, pool_size, filter_permute, p_drop, W_init=W_init):
	""" Builds the baseline U-Net model with Batch Normalization and Maxout layers after the concatenation layers. Option to permute the filters after concentation, which would result in Maxout layers where shallow convolutional layers would be competing with deeper convolutional layers in the Maxout.

	Parameters
	----------

	* input_var : Theano tensor4 symbolic variable.
		A Theano symbolic variable representing 4D float32 input.

	* input_shape : tuple type
		A 3-tuple of integers (C, H, W) where:
			* C : number of input channels
			* H : spatial height dimension
			* W : spatial width dimension 

	* pool_size : int type 
		An integer specifying the number of filters to pool together for the maxout layer.

	* filter_permute : boolean type 
		A boolean True/False indicating whether or not to randomly permute all the filters after the concatenation. 

	* p_drop : float type 
		A float type representing the amount of dropout to use for the input to the final sigmoidal classification layer.

	* W_init : Theano shared variable, expression, numpy array or callable 
		Initial value for the weights of the convolutional filters. 

	"""
	C, H, W = input_shape 

	net = OrderedDict()

	net['input'] = InputLayer(shape=(None,C,H,W), input_var=input_var)
	net.update(build_conv_bn_layer(incoming=net['input'], name='conv1', num_filters=32, filter_size=3, stride=1, pad='same', W_init=W_init))
	net.update(build_conv_bn_layer(incoming=net['conv1.nonlinearity'], name='conv2', num_filters=32, filter_size=3, stride=1, pad='same', W_init=W_init))
	net['pool1'] = MaxPool2DLayer(incoming=net['conv2.nonlinearity'], pool_size=(2,2))
	net.update(build_conv_bn_layer(incoming=net['pool1'], name='conv3', num_filters=64, filter_size=3, stride=1, pad='same', W_init=W_init))
	net.update(build_conv_bn_layer(incoming=net['conv3.nonlinearity'], name='conv4', num_filters=64, filter_size=3, stride=1, pad='same', W_init=W_init))
	net['pool2'] = MaxPool2DLayer(incoming=net['conv4.nonlinearity'], pool_size=(2,2))	
	net.update(build_conv_bn_layer(incoming=net['pool2'], name='conv5', num_filters=128, filter_size=3, stride=1, pad='same', W_init=W_init))
	net.update(build_conv_bn_layer(incoming=net['conv5.nonlinearity'], name='conv6', num_filters=128, filter_size=3, stride=1, pad='same', W_init=W_init))
	net['pool3'] = MaxPool2DLayer(incoming=net['conv6.nonlinearity'], pool_size=(2,2))
	net.update(build_conv_bn_layer(incoming=net['pool3'], name='conv7', num_filters=256, filter_size=3, stride=1, pad='same', W_init=W_init))
	net.update(build_conv_bn_layer(incoming=net['conv7.nonlinearity'], name='conv8', num_filters=256, filter_size=3, stride=1, pad='same', W_init=W_init))
	net['pool4'] = MaxPool2DLayer(incoming=net['conv8.nonlinearity'], pool_size=(2,2))
	net.update(build_conv_bn_layer(incoming=net['pool4'], name='conv9', num_filters=512, filter_size=3, stride=1, pad='same', W_init=W_init))
	net.update(build_conv_bn_layer(incoming=net['conv9.nonlinearity'], name='conv10', num_filters=512, filter_size=3, stride=1, pad='same', W_init=W_init))
	net['unpool1'] = Upscale2DLayer(incoming=net['conv10.nonlinearity'], scale_factor=2)
	net['concat1'] = ConcatLayer(incomings=[net['unpool1'], net['conv8.nonlinearity']])
	if filter_permute:
		net['permute1'] = FeaturePermutationLayer(incoming=net['concat1'])	
		net['maxout1'] = FeaturePoolLayer(incoming=net['permute1'], pool_size=pool_size)
	else:
		net['maxout1'] = FeaturePoolLayer(incoming=net['concat1'], pool_size=pool_size)
	net.update(build_conv_bn_layer(incoming=net['maxout1'], name='conv11', num_filters=256, filter_size=3, stride=1, pad='same', W_init=W_init))
	net.update(build_conv_bn_layer(incoming=net['conv11.nonlinearity'], name='conv12', num_filters=256, filter_size=3, stride=1, pad='same', W_init=W_init))
	net['unpool2'] = Upscale2DLayer(incoming=net['conv12.nonlinearity'], scale_factor=2)
	net['concat2'] = ConcatLayer(incomings=[net['unpool2'], net['conv6.nonlinearity']])
	if filter_permute:
		net['permute2'] = FeaturePermutationLayer(incoming=net['concat2'])	
		net['maxout2'] = FeaturePoolLayer(incoming=net['permute2'], pool_size=pool_size)
	else:
		net['maxout2'] = FeaturePoolLayer(incoming=net['concat2'], pool_size=pool_size)
	net.update(build_conv_bn_layer(incoming=net['maxout2'], name='conv13', num_filters=128, filter_size=3, stride=1, pad='same', W_init=W_init))
	net.update(build_conv_bn_layer(incoming=net['conv13.nonlinearity'], name='conv14', num_filters=128, filter_size=3, stride=1, pad='same', W_init=W_init))
	net['unpool3'] = Upscale2DLayer(incoming=net['conv14.nonlinearity'], scale_factor=2)
	net['concat3'] = ConcatLayer(incomings=[net['unpool3'], net['conv4.nonlinearity']])
	if filter_permute:
		net['permute3'] = FeaturePermutationLayer(incoming=net['concat3'])	
		net['maxout3'] = FeaturePoolLayer(incoming=net['permute3'], pool_size=pool_size)
	else:
		net['maxout3'] = FeaturePoolLayer(incoming=net['concat3'], pool_size=pool_size)
	net.update(build_conv_bn_layer(incoming=net['maxout3'], name='conv15', num_filters=64, filter_size=3, stride=1, pad='same', W_init=W_init))
	net.update(build_conv_bn_layer(incoming=net['conv15.nonlinearity'], name='conv16', num_filters=64, filter_size=3, stride=1, pad='same', W_init=W_init))
	net['unpool4'] = Upscale2DLayer(incoming=net['conv16.nonlinearity'], scale_factor=2)
	net['concat4'] = ConcatLayer(incomings=[net['unpool4'], net['conv2.nonlinearity']])
	if filter_permute:
		net['permute4'] = FeaturePermutationLayer(incoming=net['concat4'])	
		net['maxout4'] = FeaturePoolLayer(incoming=net['permute4'], pool_size=pool_size)
	else:
		net['maxout4'] = FeaturePoolLayer(incoming=net['concat4'], pool_size=pool_size)
	net.update(build_conv_bn_layer(incoming=net['maxout4'], name='conv17', num_filters=32, filter_size=3, stride=1, pad='same', W_init=W_init))
	net['seg_prob'] = Conv2DLayer(incoming=dropout(net['conv17.nonlinearity'], p=p_drop), num_filters=1, filter_size=1, stride=1, pad='same', W=W_init, nonlinearity=sigmoid)

	return net

def build_baseline_unet_inception_bn(input_var, input_shape, p_drop, W_init=W_init):
	""" Builds the baseline U-Net model with Batch Normalization and Inception layers consisting of 3x3 and 5x5 filters. 

	Parameters
	----------

	* input_var : Theano tensor4 symbolic variable.
		A Theano symbolic variable representing 4D float32 input.

	* input_shape : tuple type
		A 3-tuple of integers (C, H, W) where:
			* C : number of input channels
			* H : spatial height dimension
			* W : spatial width dimension 

	* p_drop : float type 
		A float type representing the amount of dropout to use for the input to the final sigmoidal classification layer.

	* W_init : Theano shared variable, expression, numpy array or callable 
		Initial value for the weights of the convolutional filters. 

	"""
	C, H, W = input_shape 

	net = OrderedDict()

	net['input'] = InputLayer(shape=(None,C,H,W), input_var=input_var)
	net.update(build_multi_scale_inception_bn_layer(incoming=net['input'], name='inception1', num_filters=[16,16], filter_sizes=[3,5], strides=[1,1], pads=['same','same'], W_init=W_init, nonlinearity=rectify))
	net.update(build_multi_scale_inception_bn_layer(incoming=net['inception1.concat'], name='inception2', num_filters=[16,16], filter_sizes=[3,5], strides=[1,1], pads=['same','same'], W_init=W_init, nonlinearity=rectify))
	net['pool1'] = MaxPool2DLayer(incoming=net['inception2.concat'], pool_size=(2,2))
	net.update(build_multi_scale_inception_bn_layer(incoming=net['pool1'], name='inception3', num_filters=[32,32], filter_sizes=[3,5], strides=[1,1], pads=['same','same'], W_init=W_init, nonlinearity=rectify))
	net.update(build_multi_scale_inception_bn_layer(incoming=net['inception3.concat'], name='inception4', num_filters=[16,16], filter_sizes=[3,5], strides=[1,1], pads=['same','same'], W_init=W_init, nonlinearity=rectify))
	net['pool2'] = MaxPool2DLayer(incoming=net['inception4.concat'], pool_size=(2,2))	
	net.update(build_multi_scale_inception_bn_layer(incoming=net['pool2'], name='inception5', num_filters=[64,64], filter_sizes=[3,5], strides=[1,1], pads=['same','same'], W_init=W_init, nonlinearity=rectify))
	net.update(build_multi_scale_inception_bn_layer(incoming=net['inception5.concat'], name='inception6', num_filters=[64,64], filter_sizes=[3,5], strides=[1,1], pads=['same','same'], W_init=W_init, nonlinearity=rectify))
	net['pool3'] = MaxPool2DLayer(incoming=net['inception6.concat'], pool_size=(2,2))
	net.update(build_multi_scale_inception_bn_layer(incoming=net['pool3'], name='inception7', num_filters=[128,128], filter_sizes=[3,5], strides=[1,1], pads=['same','same'], W_init=W_init, nonlinearity=rectify))
	net.update(build_multi_scale_inception_bn_layer(incoming=net['inception7.concat'], name='inception8', num_filters=[128,128], filter_sizes=[3,5], strides=[1,1], pads=['same','same'], W_init=W_init, nonlinearity=rectify))
	net['pool4'] = MaxPool2DLayer(incoming=net['inception8.concat'], pool_size=(2,2))
	net.update(build_multi_scale_inception_bn_layer(incoming=net['pool4'], name='inception9', num_filters=[256,256], filter_sizes=[3,5], strides=[1,1], pads=['same','same'], W_init=W_init, nonlinearity=rectify))
	net.update(build_multi_scale_inception_bn_layer(incoming=net['inception9.concat'], name='inception10', num_filters=[256,256], filter_sizes=[3,5], strides=[1,1], pads=['same','same'], W_init=W_init, nonlinearity=rectify))
	net['unpool1'] = Upscale2DLayer(incoming=net['inception10.concat'], scale_factor=2)
	net['concat1'] = ConcatLayer(incomings=[net['unpool1'], net['inception8.concat']])
	net.update(build_multi_scale_inception_bn_layer(incoming=net['concat1'], name='inception11', num_filters=[128,128], filter_sizes=[3,5], strides=[1,1], pads=['same','same'], W_init=W_init, nonlinearity=rectify))
	net.update(build_multi_scale_inception_bn_layer(incoming=net['inception11.concat'], name='inception12', num_filters=[128,128], filter_sizes=[3,5], strides=[1,1], pads=['same','same'], W_init=W_init, nonlinearity=rectify))
	net['unpool2'] = Upscale2DLayer(incoming=net['inception12.concat'], scale_factor=2)
	net['concat2'] = ConcatLayer(incomings=[net['unpool2'], net['inception6.concat']])
	net.update(build_multi_scale_inception_bn_layer(incoming=net['concat2'], name='inception13', num_filters=[64,64], filter_sizes=[3,5], strides=[1,1], pads=['same','same'], W_init=W_init, nonlinearity=rectify))
	net.update(build_multi_scale_inception_bn_layer(incoming=net['inception13.concat'], name='inception14', num_filters=[64,64], filter_sizes=[3,5], strides=[1,1], pads=['same','same'], W_init=W_init, nonlinearity=rectify))
	net['unpool3'] = Upscale2DLayer(incoming=net['inception14.concat'], scale_factor=2)
	net['concat3'] = ConcatLayer(incomings=[net['unpool3'], net['inception4.concat']])
	net.update(build_multi_scale_inception_bn_layer(incoming=net['concat3'], name='inception15', num_filters=[32,32], filter_sizes=[3,5], strides=[1,1], pads=['same','same'], W_init=W_init, nonlinearity=rectify))
	net.update(build_multi_scale_inception_bn_layer(incoming=net['inception15.concat'], name='inception16', num_filters=[32,32], filter_sizes=[3,5], strides=[1,1], pads=['same','same'], W_init=W_init, nonlinearity=rectify))
	net['unpool4'] = Upscale2DLayer(incoming=net['inception16.concat'], scale_factor=2)
	net['concat4'] = ConcatLayer(incomings=[net['unpool4'], net['inception2.concat']])
	net.update(build_multi_scale_inception_bn_layer(incoming=net['concat4'], name='inception17', num_filters=[16,16], filter_sizes=[3,5], strides=[1,1], pads=['same','same'], W_init=W_init, nonlinearity=rectify))
	net['seg_prob'] = Conv2DLayer(incoming=dropout(net['inception17.concat'], p=p_drop), num_filters=1, filter_size=1, stride=1, pad='same', W=W_init, nonlinearity=sigmoid)

	return net

def build_baseline_unet_inception_bn_maxout(input_var, input_shape, pool_size, filter_permute, p_drop, W_init=W_init):
	""" Builds the baseline U-Net model with Batch Normalization and Inception layers consisting of 3x3 and 5x5 filters and Maxout layers after the concatenation layers. 

	* Note: this does not seem to outperform the U-Net-Inception-BN network, though it does cut down the training time due to filter-wise dimensionality reduction after the Maxout.

	Parameters
	----------

	* input_var : Theano tensor4 symbolic variable.
		A Theano symbolic variable representing 4D float32 input.

	* input_shape : tuple type
		A 3-tuple of integers (C, H, W) where:
			* C : number of input channels
			* H : spatial height dimension
			* W : spatial width dimension 

	* pool_size : int type 
		An integer specifying the number of filters to pool together for the maxout layer.

	* filter_permute : boolean type 
		A boolean True/False indicating whether or not to randomly permute all the filters after the concatenation. 

	* p_drop : float type 
		A float type representing the amount of dropout to use for the input to the final sigmoidal classification layer.

	* W_init : Theano shared variable, expression, numpy array or callable 
		Initial value for the weights of the convolutional filters. 

	"""
	C, H, W = input_shape 

	net = OrderedDict()

	net['input'] = InputLayer(shape=(None,C,H,W), input_var=input_var)
	net.update(build_multi_scale_inception_bn_layer(incoming=net['input'], name='inception1', num_filters=[16,16], filter_sizes=[3,5], strides=[1,1], pads=['same','same'], W_init=W_init, nonlinearity=rectify))
	net.update(build_multi_scale_inception_bn_layer(incoming=net['inception1.concat'], name='inception2', num_filters=[16,16], filter_sizes=[3,5], strides=[1,1], pads=['same','same'], W_init=W_init, nonlinearity=rectify))
	net['pool1'] = MaxPool2DLayer(incoming=net['inception2.concat'], pool_size=(2,2))
	net.update(build_multi_scale_inception_bn_layer(incoming=net['pool1'], name='inception3', num_filters=[32,32], filter_sizes=[3,5], strides=[1,1], pads=['same','same'], W_init=W_init, nonlinearity=rectify))
	net.update(build_multi_scale_inception_bn_layer(incoming=net['inception3.concat'], name='inception4', num_filters=[16,16], filter_sizes=[3,5], strides=[1,1], pads=['same','same'], W_init=W_init, nonlinearity=rectify))
	net['pool2'] = MaxPool2DLayer(incoming=net['inception4.concat'], pool_size=(2,2))	
	net.update(build_multi_scale_inception_bn_layer(incoming=net['pool2'], name='inception5', num_filters=[64,64], filter_sizes=[3,5], strides=[1,1], pads=['same','same'], W_init=W_init, nonlinearity=rectify))
	net.update(build_multi_scale_inception_bn_layer(incoming=net['inception5.concat'], name='inception6', num_filters=[64,64], filter_sizes=[3,5], strides=[1,1], pads=['same','same'], W_init=W_init, nonlinearity=rectify))
	net['pool3'] = MaxPool2DLayer(incoming=net['inception6.concat'], pool_size=(2,2))
	net.update(build_multi_scale_inception_bn_layer(incoming=net['pool3'], name='inception7', num_filters=[128,128], filter_sizes=[3,5], strides=[1,1], pads=['same','same'], W_init=W_init, nonlinearity=rectify))
	net.update(build_multi_scale_inception_bn_layer(incoming=net['inception7.concat'], name='inception8', num_filters=[128,128], filter_sizes=[3,5], strides=[1,1], pads=['same','same'], W_init=W_init, nonlinearity=rectify))
	net['pool4'] = MaxPool2DLayer(incoming=net['inception8.concat'], pool_size=(2,2))
	net.update(build_multi_scale_inception_bn_layer(incoming=net['pool4'], name='inception9', num_filters=[256,256], filter_sizes=[3,5], strides=[1,1], pads=['same','same'], W_init=W_init, nonlinearity=rectify))
	net.update(build_multi_scale_inception_bn_layer(incoming=net['inception9.concat'], name='inception10', num_filters=[256,256], filter_sizes=[3,5], strides=[1,1], pads=['same','same'], W_init=W_init, nonlinearity=rectify))
	net['unpool1'] = Upscale2DLayer(incoming=net['inception10.concat'], scale_factor=2)
	net['concat1'] = ConcatLayer(incomings=[net['unpool1'], net['inception8.concat']])
	if filter_permute:
		net['permute1'] = FeaturePermutationLayer(incoming=net['concat1'])	
		net['maxout1'] = FeaturePoolLayer(incoming=net['permute1'], pool_size=pool_size)
	else:
		net['maxout1'] = FeaturePoolLayer(incoming=net['concat1'], pool_size=pool_size)
	net.update(build_multi_scale_inception_bn_layer(incoming=net['maxout1'], name='inception11', num_filters=[128,128], filter_sizes=[3,5], strides=[1,1], pads=['same','same'], W_init=W_init, nonlinearity=rectify))
	net.update(build_multi_scale_inception_bn_layer(incoming=net['inception11.concat'], name='inception12', num_filters=[128,128], filter_sizes=[3,5], strides=[1,1], pads=['same','same'], W_init=W_init, nonlinearity=rectify))
	net['unpool2'] = Upscale2DLayer(incoming=net['inception12.concat'], scale_factor=2)
	net['concat2'] = ConcatLayer(incomings=[net['unpool2'], net['inception6.concat']])
	if filter_permute:
		net['permute2'] = FeaturePermutationLayer(incoming=net['concat2'])	
		net['maxout2'] = FeaturePoolLayer(incoming=net['permute2'], pool_size=pool_size)
	else:
		net['maxout2'] = FeaturePoolLayer(incoming=net['concat2'], pool_size=pool_size)
	net.update(build_multi_scale_inception_bn_layer(incoming=net['maxout2'], name='inception13', num_filters=[64,64], filter_sizes=[3,5], strides=[1,1], pads=['same','same'], W_init=W_init, nonlinearity=rectify))
	net.update(build_multi_scale_inception_bn_layer(incoming=net['inception13.concat'], name='inception14', num_filters=[64,64], filter_sizes=[3,5], strides=[1,1], pads=['same','same'], W_init=W_init, nonlinearity=rectify))
	net['unpool3'] = Upscale2DLayer(incoming=net['inception14.concat'], scale_factor=2)
	net['concat3'] = ConcatLayer(incomings=[net['unpool3'], net['inception4.concat']])
	if filter_permute:
		net['permute3'] = FeaturePermutationLayer(incoming=net['concat3'])	
		net['maxout3'] = FeaturePoolLayer(incoming=net['permute3'], pool_size=pool_size)
	else:
		net['maxout3'] = FeaturePoolLayer(incoming=net['concat3'], pool_size=pool_size)
	net.update(build_multi_scale_inception_bn_layer(incoming=net['maxout3'], name='inception15', num_filters=[32,32], filter_sizes=[3,5], strides=[1,1], pads=['same','same'], W_init=W_init, nonlinearity=rectify))
	net.update(build_multi_scale_inception_bn_layer(incoming=net['inception15.concat'], name='inception16', num_filters=[32,32], filter_sizes=[3,5], strides=[1,1], pads=['same','same'], W_init=W_init, nonlinearity=rectify))
	net['unpool4'] = Upscale2DLayer(incoming=net['inception16.concat'], scale_factor=2)
	net['concat4'] = ConcatLayer(incomings=[net['unpool4'], net['inception2.concat']])
	if filter_permute:
		net['permute4'] = FeaturePermutationLayer(incoming=net['concat4'])	
		net['maxout4'] = FeaturePoolLayer(incoming=net['permute4'], pool_size=pool_size)
	else:
		net['maxout4'] = FeaturePoolLayer(incoming=net['concat4'], pool_size=pool_size)
	net.update(build_multi_scale_inception_bn_layer(incoming=net['maxout4'], name='inception17', num_filters=[16,16], filter_sizes=[3,5], strides=[1,1], pads=['same','same'], W_init=W_init, nonlinearity=rectify))
	net['seg_prob'] = Conv2DLayer(incoming=dropout(net['inception17.concat'], p=p_drop), num_filters=1, filter_size=1, stride=1, pad='same', W=W_init, nonlinearity=sigmoid)

	return net

if __name__ == '__main__':
	pass 