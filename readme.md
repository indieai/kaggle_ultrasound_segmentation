# Kaggle Ultrasound Nerve Segmentation Competition 

A repository for the [Kaggle Ultrasound Nerve Segmentation Competition](https://www.kaggle.com/c/ultrasound-nerve-segmentation).

## Modules

This repository consists of the following core components:

**Data Load**

- create_train : a function which loads the training data and saves a .npy file for more efficient data loading.
- create_test : a function which loads the test data and saves a .npy file for more efficient data loading.
- load_train : a function which loads the training data .npy file  
- load_test : a function which loads the test data .npy file  

**Data Processing**
A series of functions which processes the training and test data. Some operations are:

* Resizing the images.
* Perform global contrast normalization on the train and test set.
* Data augmentation: horizontal and vertical flips as well as multiscale random crops. 

In general we perform a lot of data augmentation, as there is usually a dearth of labeled data in medical imaging. 

**Utils**

- minibatch iterator
- dice loss function
- a trainer function 
- a run length encoder function 
- a function which extracts predictions and saves a Kaggle submission file

**Models and custom modules**

- custom FeaturePermutationLayer Lasagne layer
- custom Inception-BN convolutional layer 
- Baseline U-Net network  
- U-Net-BN network 
- U-Net-BN-Sigmoidal Pooling network
- U-Net-Maxout-BN network
- U-Net-Inception-BN network 
- U-Net-Inception-Maxout-BN network 

## Dependencies

All experiments were performed using the following setup:

* Ubuntu 14.04
* GeForce 960 4GB GPU, Intel i5-4570 CPU 3.2Ghz, 16 GB of RAM
* Python 2.7.6
* NumPy 1.10.4
* Theano 0.8.0.dev-1efb15394850a1cf08c7e26eea6fec256634aa7a
* Lasagne 0.2.dev1
* cuDNN 3007 (Major: 3, Minor: 0, Patch Level: 07)
* natsort 5.0.1
* cv2 2.4.8

## Instructions 

To run the code, perform the following: 

1. In the data_load.py file, input the directory path for the Kaggle data set. 
2. Run the data_load.py file to output train and test .npy files for easy loading of the data.
3. Define the weights_dir in the benchmarking.py file and then run the benchmarking.py file to see how the various models perform on an 80/20 train/val split with an 8X data augmentation increase of the training set. 
4. Define the weights_dir in the train_inception_model.py file and the train_maxout_model.py file and run those scripts.
5. Define the maxout_index and inception_index in the ensemble.py file. Run the ensemble.py file to ensemble the U-Net-Maxout-BN and the U-Net-Inception-BN models together and save a Kaggle submission file.