""" This is a script used to train a U-Net-BN network. 
"""
from data_process.data_process import full_data_augmentation, random_crop
from data_process.data_load import load_train, load_test
from utils.utils import *
from models.networks import build_baseline_unet_bn
import theano, os, sys
import theano.tensor as T
from lasagne.layers import get_output, get_all_params, set_all_param_values
from lasagne.updates import adam

#################################################
##### Initialization and data preprocessing #####
#################################################

# Define some parameters:
h_reshape, w_reshape = (64, 80)
input_shape = (1, h_reshape, w_reshape)

# Load the data:
X_train, Y_train = load_train()
X_test = load_test()

# Process the data and perform data augmentation:
val_ratio = 0 # We are training on the entire dataset. 
X_train, Y_train, X_val, Y_val, X_test = full_data_augmentation(X_train, Y_train, X_test, val_ratio, new_shape=(h_reshape,w_reshape), aux_shapes=[(h_reshape+4, w_reshape+4), (h_reshape+8, w_reshape+8)])

# Note: our data augmentation schemes goes as follows:
# 1) X_train resized to (h_reshape, w_reshape) + horizontal and vertical flips
# 2) X_train resized to (h_reshape+4, w_reshape+4) + horizontal and vertical 
# flips
# 3) X_train resized to (h_reshape+8, w_reshape+8) + horizontal and vertical 
# flips
# Resulting in an 8X data augmentation. 

data_dict = OrderedDict()
data_dict['X_train'], data_dict['Y_train'] = X_train, Y_train
data_dict['X_val'], data_dict['Y_val'] = X_val, Y_val

# Define the training parameters:
num_epochs = 50
batchsize = 32
val_batchsize = 10
reg = 0.001
lr = 1e-4
crop = True
crop_padding = (4,4)
p_drop = 0.1
pool_size = 2
filter_permute = False

# Define a weights directory to save the weights, the training diagnostics as well as the CSV Kaggle submission file:
weights_dir = '/insert_weights_dir/'

# Build the update function:
def adam_func(loss_or_grads, params):
	return adam(loss_or_grads, params, learning_rate=lr)

# Initialize Theano symbolic variables:
input_var = T.tensor4('inputs')
target_var = T.tensor4('targets', dtype='int8')

##############################
##### Train the U-Net-BN #####
##############################
print("*"*30)
print("Training the U-Net-BN network...")
print("*"*30)
# Initialize a network:
net = build_baseline_unet_bn(input_var, input_shape, p_drop)
save_dir = weights_dir+'unet_bn/'

# Train it! 
train_snapshot = trainer(net, input_var, target_var, data_dict, dice_loss, adam_func, save_dir, num_epochs, batchsize, val_batchsize, reg, crop, crop_padding)

# Create a submission: 
submissions(save_dir, net, input_var, X_test)