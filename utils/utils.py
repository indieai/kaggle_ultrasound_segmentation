import numpy as np
import theano, time, os, sys, csv, cv2
import theano.tensor as T
from lasagne.layers import get_all_params, get_all_param_values, get_output
from lasagne.layers import get_all_layers
from lasagne.regularization import regularize_layer_params, l2
from collections import OrderedDict
import cPickle as pickle

def iterate_minibatches(inputs, targets, batchsize, shuffle=False):
	""" A minibatch iterator. 

	Parameters
	----------
	* inputs : numpy array 
		Input dataset. 

	* targets : numpy array 
		Labels of the input dataset.

	* batchsize : int 
		An integer specifying the size of each minibatch 

	* shuffle : boolean True/False 
		True/False as to whether or not we will be shuffling the data each time we complete a full pass. 

	"""
	assert len(inputs) == len(targets)
	if shuffle:
		indices = np.arange(len(inputs))
		np.random.shuffle(indices)
	for start_idx in range(0, len(inputs) - batchsize + 1, batchsize):
		if shuffle:
			excerpt = indices[start_idx:start_idx + batchsize]
		else:
			excerpt = slice(start_idx, start_idx + batchsize)
		yield inputs[excerpt], targets[excerpt]

def compute_dice_coefficient(predictions, targets, smoothing_factor=1.0):
	""" Computes a smoothed dice coefficient. 

	Parameters
	----------

	* predictions : Theano symbolic variable.
		A Theano tensor4 symbolic variable representing the predicted mask. 

	* targets : Theano symbolic variable.
		A Theano tensor4 symbolic variable representing the true mask. 

	* smoothing_factor : float type
		A float representing a smoothing factor in the dice coefficient so as to avoid division by 0 while also allowing the network to output a dice coefficient score exactly equal to 1.

	"""

	predictions_flat, targets_flat  = T.flatten(predictions), T.flatten(targets)
	return (2.0*T.sum(predictions_flat * targets_flat) + smoothing_factor)/(T.sum(predictions_flat) + T.sum(targets_flat) + smoothing_factor)

def dice_loss(predictions, targets, smoothing_factor=1.0):
	""" Computes a dice loss function. It is positive and takes values in the unit interval [0,1]. 

	Parameters
	----------

	* predictions : Theano symbolic variable.
		A Theano tensor4 symbolic variable representing the predicted mask. 

	* targets : Theano symbolic variable.
		A Theano tensor4 symbolic variable representing the true mask. 

	* smoothing_factor : float type
		A float representing a smoothing factor in the dice coefficient so as to avoid division by 0 while also allowing the network to output a dice coefficient score exactly equal to 1.
	"""

	dice_coeff = compute_dice_coefficient(predictions, targets, smoothing_factor)
	return 1 - dice_coeff

def random_crop(X, Y, crop_size, pad=(0,0)):
	""" A function that takes as input 4D tensors X and Y representing  training images and training masks, respectively, and returns a random crops of each. 

	Parameters
	----------

	* X, Y : numpy array type 
		Input dataset of shape (N, C, H, W) where:
			* N : number of data points
			* C : number of image channels 
			* H : spatial height of the image 
			* W : spatial width of the image 

	* crop_size : tuple type 
		Crop size. 

	* pad: tuple type
		Spatial padding. Default is set to (0,0), i.e. no padding.
	"""
	# Pad the data:
	if pad != (0,0):
		X = np.lib.pad(X, ((0,0),(0,0),(pad[0],pad[0]), (pad[1],pad[1])), mode='constant')
		Y = np.lib.pad(Y, ((0,0),(0,0),(pad[0],pad[0]), (pad[1],pad[1])), mode='constant')

	N, C, H, W = X.shape 
	h, w = crop_size

	assert (h < H and w < W), "Crop dimensions cannot exceed original image dimensions." 

	X_random_crops = np.zeros((N,C,h,w), dtype=theano.config.floatX)
	Y_random_crops = np.zeros((N,C,h,w), dtype='int8')

	# Choose a random corner point in a vectorized fashion:
	corners = np.random.random_integers(low=0, high=H-h, size=(N,2))

	for i in xrange(N):
		X_random_crops[i] = X[i, :, corners[i,0]:corners[i,0]+h, corners[i,1]:corners[i,1]+w]
		Y_random_crops[i] = Y[i, :, corners[i,0]:corners[i,0]+h, corners[i,1]:corners[i,1]+w]

	return X_random_crops, Y_random_crops

def trainer(net, input_var, target_var, data_dict, objective_func, update_func, save_dir, num_epochs=100, batchsize=32, val_batchsize=10, reg=1e-5, crop=True, crop_padding=(4,4)):
	""" A training function. 

	Parameters
	----------

	* net : OrderedDict type
		An OrderedDict containing all the layers for the network. This network must be randomly initialized.

	* input_var : Theano symbolic variable type 
		A Theano symbolic variable representing the input to the network.

	* target_var : Theano symbolic variable type 
		A Theano symbolic variable representing the targets of the network.

	* data_dict: OrderedDict type
		An OrderedDict containing all the training and validation data. Keys are: ['X_train', 'Y_train', 'X_val', 'Y_val']. Each of these are 4D tensors of shape (N, C, H, W) where:
			* N : number of data points, respectively
			* C : number of image channels 
			* H : spatial height of the image 
			* W : spatial width of the image 

	* objective_func : function type 
		A function type indicating the objective function to be minimized. This function must take two input arguments:
				objective_func(predictions, targets).

	* update_func : function type 
		A function type indicating the update rule to be used. Assumed to already have its learning rate and any other optimization parameters to be set, only input will be the objective function and the params. This function must take two input arguments: 
				update_func(loss_or_grads, params).

	* save_dir : string type
		A string specifying the file directory to save the weights of the network.

	* num_epochs : int type
		An integer specifying the number of training epochs. Default is set to 100. 

	* batchsize : int type 
		An integer specifying the size of each training batch. Default is set to 32.

	* val_batchsize: int type 
		An integer specifying the size of each val batch. Default is set to 10.

	* reg : float type 
		A float specifying the l2 regularization parameter value. Default is set to 1e-5.

	* crop: boolean type 
		A boolean True/False indicating whether or not to take random crops. Default is set to True.

	* crop_padding : tuple type
		A 2-tuple of integers indicating the amount of zero padding to use for the random cropping. Default is set to (4,4).

	"""
	# Check if the save directory exists, if not create it:
	if os.path.exists(save_dir) == False:
		os.makedirs(save_dir)

	# Unpack the data_dict:
	X_train = data_dict['X_train']
	Y_train = data_dict['Y_train']
	X_val = data_dict['X_val']
	Y_val = data_dict['Y_val']

	# Define the shape of the images:
	num_train = X_train.shape[0]
	H, W = X_train[0,0].shape

	# Define the out key, i.e. the name of the last layer of the network:
	out_key = net.keys()[-1]

	# Save the initialized weights:
	np.savez(os.path.join(save_dir, '0_iter_completed_weights.npz'), *get_all_param_values(net[out_key]))

	# Define output, loss, update, train_fn, test_fn
	print("*"*50)
	print("Building training and test functions...")
	print("*"*50)
	train_out = get_output(net[out_key])
	train_loss = objective_func(predictions=train_out, targets=target_var)
	train_loss = train_loss.mean()
	all_layers = get_all_layers(net[out_key])
	l2_penalty = reg * regularize_layer_params(all_layers, l2)
	train_loss = train_loss + l2_penalty 
	params = get_all_params(net[out_key], trainable=True)
	updates = update_func(loss_or_grads=train_loss, params=params)
	train_fn = theano.function([input_var, target_var], train_loss, updates=updates)
	test_out = get_output(net[out_key], deterministic=True)
	test_loss = objective_func(predictions=test_out, targets=target_var)
	test_loss = test_loss.mean()
	test_fn = theano.function([input_var, target_var], test_loss)

	# Define the number of train and val batches:
	num_train_batches = X_train.shape[0]/batchsize
	if X_val.shape[0] == 0:
		num_val_batches = 1
	else:
		num_val_batches = X_val.shape[0]/val_batchsize

	# Initialize some counters we will use during training:
	max_iter = num_epochs*num_train_batches
	iter_count = 0
	val_count = 0

	# Initialize some diagnostic arrays:
	train_loss_arr = np.zeros((max_iter))
	train_loss_epoch_arr = np.zeros((num_epochs))
	val_loss_epoch_arr = np.zeros((num_epochs))

	print("*"*50)
	print("Officer Hoyt, today is your training day...")
	print("*"*50)
	for i in xrange(num_epochs):
		# Learning rate decay schedules: 

		train_loss_value = 0.0
		epoch_time = time.time()
		point = num_train_batches/100
		point_float = num_train_batches/100.0
		increment = num_train_batches/50
		for index, iter_vals in enumerate(iterate_minibatches(X_train, Y_train, batchsize, shuffle=True)):
			inputs, targets = iter_vals
			if index == 0:
				batch_time = time.time()
			if crop:
				inputs, targets = random_crop(X=inputs, Y=targets, crop_size=(H,W), pad=crop_padding)
			train_loss_arr[iter_count] = train_fn(inputs, targets)
			if index == 0:
				batch_time = time.time() - batch_time
			train_loss_value += train_loss_arr[iter_count]

			if(index % (2*point) == 0):
				sys.stdout.write("  ETA: %.2fs, Epoch %d, batch %d of %d, train loss %f \r[" % ((num_train_batches - index)*batch_time, i+1, index, num_train_batches, train_loss_value/index) + "=" * (index / increment) +  " " * ((num_train_batches - index)/ increment) + "] " +  "%.2f" % (index / point_float) + "%  ")
				sys.stdout.flush()
			if index+1 == num_train_batches:

				# Evaluate on the validation set:
				val_loss_value = 0
				for inputs, targets in iterate_minibatches(X_val, Y_val, val_batchsize, shuffle=False):
					val_loss_batch = test_fn(inputs, targets)
					val_loss_value += val_loss_batch
				val_loss_epoch_arr[i] = val_loss_value/num_val_batches
				train_loss_epoch_arr[i] = train_loss_value/float(num_train_batches)
				val_loss_epoch_arr[i] = val_loss_value/num_val_batches

				sys.stdout.write("  Epoch %d completed in %.2f seconds, train loss %f, val loss %f, val dice score: %f \r[" % (i+1, time.time()-epoch_time, train_loss_epoch_arr[i], val_loss_epoch_arr[i], -1*(val_loss_epoch_arr[i]-1)) + "=" * (index / increment) +  " " * ((num_train_batches - index)/ increment) + "] " +  str(100) + "%  ")
				sys.stdout.write("\n")

		# Save the weights after each epoch:
		np.savez(os.path.join(save_dir, '%d_epochs_completed_weights.npz' % (i+1)), *get_all_param_values(net[out_key]))

	# Take a training snapshot:
	train_snapshot = OrderedDict()
	train_snapshot['iter_count'] = iter_count
	train_snapshot['num_epochs'] = num_epochs
	train_snapshot['train_loss_arr'] = train_loss_arr
	train_snapshot['train_loss_epoch_arr'] = train_loss_epoch_arr 
	train_snapshot['val_loss_epoch_arr'] = val_loss_epoch_arr

	pickle.dump(train_snapshot, open(os.path.join(save_dir, 'train_snapshot.pkl'), 'wb'))

	print("*"*50)
	print("Officer Hoyt, your training day is complete. Best recorded validation dice score was: %.4f." % (-1*(val_loss_epoch_arr.min()-1)))
	print("*"*50)

	return train_snapshot

def run_length_enc(label):
	""" A function which will take a binary valued numpy array and return its run length encoding. 

	Reference
	---------

	* Code reference: This function is directly copied from Jocic Marko Keras U-Net implementation: https://github.com/jocicmarko/ultrasound-nerve-segmentation. 

	"""
	from itertools import chain
	x = label.transpose().flatten()
	y = np.where(x > 0)[0]
	if len(y) < 10:  # consider as empty
		return ''
	z = np.where(np.diff(y) > 1)[0]
	start = np.insert(y[z+1], 0, y[0])
	end = np.append(y[z], y[-1])
	length = end - start
	res = [[s+1, l+1] for s, l in zip(list(start), list(length))]
	res = list(chain.from_iterable(res))
	return ' '.join([str(r) for r in res])

def submissions(save_dir, net, input_var, X_test, original_shape=(420,580)):
	""" A function which will extract mask predictions from a given input network. If the net was trained on resized image data, this function will resize the predicted masks to the appropriate shape.

	Parameters
	----------

	* save_dir : string type
		A string specifying the file directory to save the CSV file.
	* net : 

	* input_var : 

	* X_test : numpy array type 
		A 4D tensor of the predicted masks. Has shape (N, C, H, W) where:
			* N : number of data points
			* C : number of image channels 
			* H : spatial height of the image 
			* W : spatial width of the image 

	* original_shape : tuple type 

	""" 

	test_out = get_output(net['seg_prob'], deterministic=True)
	get_out = theano.function([input_var], test_out)
	H, W = original_shape
	N, C, h, w = X_test.shape

	print("*"*50)
	print("Predicting on the test set...")
	print("*"*50)
	# Predict on X_test:
	predicted_masks = np.zeros((0,C,h,w), dtype='uint8')
	Y_test_dummy = np.zeros((N,C,h,w), dtype='uint8')
	for batch in iterate_minibatches(X_test, Y_test_dummy, 36, shuffle=False):
		test_img_batch, test_mask_dummy_batch = batch
		mask_predictions_batch = get_out(test_img_batch)
		predicted_masks = np.concatenate((predicted_masks, mask_predictions_batch),axis=0)
	print("*"*50)
	print("Preparing a Kaggle submission...")
	print("*"*50)
	point = N/100
	increment = N/50
	# Convert the predicted test masks into run length encoding format:
	with open(os.path.join(save_dir, 'submission.csv'), 'wb') as csvfile:
		kaggle_submission = csv.writer(csvfile, delimiter=',')
		kaggle_submission.writerow(['img', 'pixels'])
		for index, img in enumerate(predicted_masks):
			img = img.squeeze()
			img = cv2.threshold(img, 0.5, 1., cv2.THRESH_BINARY)[1].astype('uint8')
			img = cv2.resize(img, (W, H), interpolation=cv2.INTER_CUBIC)
			rle = run_length_enc(img)

			# Write to the submission.csv file:
			kaggle_submission.writerow(['%d' % (index+1), rle])

			if(index % (2*point) == 0):
				sys.stdout.write("  Test image %d of %d \r[" % (index+1, N) + "=" * (index / increment) +  " " * ((N - index)/ increment) + "] " +  str(index / point) + "% ")
				sys.stdout.flush()

		print("Kaggle CSV submission file ready.")

if __name__ == '__main__':
	pass 